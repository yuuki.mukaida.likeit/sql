﻿SELECT 
    a.category_name, 
    SUM(b.item_price) AS total_price 
FROM 
    item_category a 
INNER JOIN 
    item b 
ON 
    a.category_id = b.category_id 
GROUP BY 
    a.category_id 
ORDER BY 
    b.item_price DESC;

