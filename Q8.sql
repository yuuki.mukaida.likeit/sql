﻿SELECT 
    a.item_id, 
    a.item_name, 
    a.item_price, 
    b.category_name 
FROM 
    item a 
INNER JOIN 
    item_category b 
ON 
    a.category_id = b.category_id;

